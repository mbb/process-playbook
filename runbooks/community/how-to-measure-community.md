Per channel:
Search-engine Rankings
Monthly amount of searches / impressions (just shown in results, not even acted on)
Views

If content creation = important, analyze. If not, use your time differently. (Matt’s opinion: SEO is better)
What about content consumption? Not just its creation.
Are people acting on the consumption? CTA conversion?

What kind of model of product do you have to support?

## Graphing the need

Note that phase of the project (early, released) and type of product (SaaS vs HW vs API)
Levels of easy of Advocacy:
Example app = immediate sale
Salesforce example - get people hooked on the API functionality and help the upsell and/or stickiness of use of SF

Notes: think through how crappy the project is at this phase (use better wording)
Early on - Feedback
Maturing - Adoption


## Language that helps

* Share of Voice (SOV)
* Same-breath awareness - a phrase I have found to be very powerful when presenting why community is perfect for "Thought Leadership" pieces
* Planned vs Organic - way of showing that community engagement has an X factor to it of unpredictable value.


To read:
https://opensource.com/business/16/3/resources-measuring-open-source-community-roi
