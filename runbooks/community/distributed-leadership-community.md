| Need                             | Description                                                            | Champion | Notes | Best Link |
|:---------------------------------|:-----------------------------------------------------------------------|:---------|:------|:----------|
| Introduction Slides              | Champions the best story for an introduction to X                      |          |       |           |
| User Personas                    | Champions the understanding of who uses the Project                    |          |       |           |
| Mascot Images                    | Has access to and champions the use of X                               |          |       |           |
| List of Users                    | Champions the latest list of project users                             |          |       |           |
| Competitive Landscape            | Champions the understanding of competitive projects                    |          |       |           |
| Distinctive Competence           | Champions the latest list of what makes X special                      |          |       |           |
| Market Analysis                  | Champions the understanding of the TAM based on target users           |          |       |           |
| Roadmap                          | Champions the sharing of the project roadmap                           |          |       |           |
| Business Plan                    | Champions attaching project adoption to revenue                        |          |       |           |
| Partner List                     | Champions the latest list and how they support the project             |          |       |           |
| Use Cases                        | Champions understanding of architectures that benefit from the project |          |       |           |
| Marketing Plan                   | Champions the holistic view of how we talk about the project           |          |       |           |
| User Acquisition                 | Champions the playbook we follow to attract new users                  |          |       |           |
| User Retention                   | Champions the point of view of keeping users active in the project     |          |       |           |
| Project Support                  |                                                                        |          |       |           |
| Release Process                  |                                                                        |          |       |           |
| Marketing Collateral             |                                                                        |          |       |           |
| Sales Enablement                 |                                                                        |          |       |           |
| Customer References              |                                                                        |          |       |           |
| Internal Collaboration           | Champion effort to expose company-specific goals                       |          |       |           |
| Plugin Development in Language X |                                                                        |          |       |           |
| Best Demo                        |                                                                        |          |       |           |
| Best Presentations               |                                                                        |          |       |           |
| Event Support                    |                                                                        |          |       |           |



