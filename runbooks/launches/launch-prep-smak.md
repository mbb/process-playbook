# Social Media Activation Kit
Empower everyone to be more social.

## Timeline
Map out the timeline.

| Phase                 | Audience | Key CTAs and Examples |
|:----------------------|:--------:|:----------------------|
| Teaser                |          |                       |
| Launch Date: 11/11/11 |          |                       |
| Sustainer             |          |                       |

**Considerations:**

* Does the messaging change over time? What are the key dates?
* Have a few segments to the timeline? Call them *PHASES*

##Channels

### The Options

For **personal accounts:**

* Twitter
* Facebook & LinkedIn - I think of them in the same bucket
* Reddit & Hacker News- Target subreddits, write down headlines

For **corporate accounts:**

* Twitter - draft compelling tweets from our accounts, find excuses to '@' influencers who may be interested. Don't forget to use images!
* Facebook Group
* LinkedIn Group
* Adjacent LinkedIn Groups - are there frequented groups that may benefit from this conversation?

### Targets

- [ ] $CHANNEL
- [ ] $CHANNEL

**Considerations:**

* Custom messaging to specific audiences? (By Geo, Persona or expertise)
