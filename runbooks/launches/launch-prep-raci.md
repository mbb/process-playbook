# Launch Preparation
Create a checklist with clear ownership and decision making. I often use [RACI](../models/RACI.md) to do so.

| Events                                    | Project Executive | Project Manager | Team Specialist | OSS Developers | x-Org Specialist | Marketing Lead | Legal |
|:-------------------------------|:-----------------:|:---------------:|:---------------:|:--------------:|:----------------:|:--------------:|:-----:|
| Align timeline to code complete           |                   |                 |                 |                |                  |                |       |
| Decide key audience & messages            |                   |                 |                 |                |                  |                |       |
| Gather technical diagrams                 |                   |                 |                 |                |                  |                |       |
| Create a demo as OSS download             |                   |                 |                 |                |                  |                |       |
| Create a demo for events                  |                   |                 |                 |                |                  |                |       |
| Verify demos functionality & instructions |                   |                 |                 |                |                  |                |       |
| Create $OTHER_CONTENT                     |                   |                 |                 |                |                  |                |       |
| Create $OTHER_CONTENT                     |                   |                 |                 |                |                  |                |       |
| Share SMAK with all teams                 |                   |                 |                 |                |                  |                |       |
| Publish OSS demo                          |                   |                 |                 |                |                  |                |       |
| Monitor social media reactions            |                   |                 |                 |                |                  |                |       |

## Tips

You now have a reasonable mapping of the teams and process involved to bring your launch to completion. The PM should also feel empowered to make decisions without a huge meeting to move every decision forward. Here are some notes I've written down for myself that make this much more effective:

1. The most common conversation this sparks is one of **defining the goal**. What does success even look like? This process is worth its weight for that point alone
1. Events work best when they're specific deliverables -- the *completion* of a task or the *sharing* of a document
2. **Define your channels** -- when sharing a document, what does that mean? Emailing it? Posting to a site? It's not complete until you take this action
3. Stuck on what events should be written down? Work your way backwards: what does success look like for this launch? I'm even okay with vague thoughts like "virality," since it sets an expectation for the tone (edgy or funny) and channels (thinking beyond email).
4. Help people feel okay with this separation of roles by giving them rich content. Map out events through a visual timeline (something as simple as a screenshot of your PPT slide)
5. How do you know those Responsible are empowered? Ask these three questions by Albert Bandura:
	1. Can you do it?
	2. Will it work?
	3. Is it worth it?
