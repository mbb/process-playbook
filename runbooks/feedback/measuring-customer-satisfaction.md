# Novice

## Health of the Survey

1. Are customers’ expectations being met when they talk to support?
1. What has the workload been like?
1. What has customer activity been like?
1. Have there been any outliers recently?
1. What have our response times been like?
1. How likely are customers to recommend us to their friends and colleagues?
1. How much effort do customers expend when they solve their problems with us?

https://www.helpscout.net/blog/customer-satisfaction/
