# Usability Tests

## Why?

> Usability testing requires more upfront planning, but far and away delivers more insights than any of the methods listed here. It uncovers things customers sometimes don’t know they're thinking about or struggling with, and usually provides you with a clear path to make the experience better.
  ~ [link](https://www.helpscout.net/blog/customer-feedback/)

## When

NOT good for incomplete work. Think 90% done, not 50%.

## Remember

Make it worth the time! Give swag, free access, or at least public praise for doing so.


No time to manage it? Use this: https://www.usertesting.com/
