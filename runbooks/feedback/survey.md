# Surveying users

## Making sure it's the right option

If you don't need systematic feedback, think about other options:

* Exploratory [customer interviews](./exploratory-interview.md)
* Review existing customer interactions (respect the data you have!)

## How

How to write questions:

* Ask only questions that fulfill your end goal 
* Construct smart, open-ended questions
* Ask one question at a time 
* Make rating scales consistent 
* Avoid leading and loaded questions  

More detailed tips in [exploratory interviews](./exploratory-interview.md#writing-non-biased-questions)

## Tools

This is a short list of tools that you may find useful to help you with your customer development interviews, information synthesis, and sharing information with your team.

### Find People

Use your friends by sharing something like this:

    I’m trying to learn more about [topic]
    Can you introduce me to [type of person] so I can ask them a few quick questions?
    Here’s a message you can forward:
    [Name] suggested that you would be a good person to help me learn about [topic].
    Could you help me out by having a 15-minute call with me?  It’d be a huge help for me
    to learn from your experiences.

    Thanks,
    [name and contact info]

### Search online communities

* LinkedIn
    * find people with the right job title, in the right industry, with relevant expertise
* Quora
    * find people posting on the right topics, following the right topics/questions
* Medium (google ‘topic name’ site:medium.com)
    * find people writing essays on stuff you care about
* Twitter (best for finding #topics and sites than connecting with people)

### Sketch

* Invision ([http://www.invisionapp.com](http://www.invisionapp.com))
    * really easy to tie together mockups into a click-through demo
* The Noun Project ([http://www.nounproject.com](http://www.nounproject.com))
    * simple iconic images for nouns

### Ask Questions Without a Website

* Wufoo ([http://www.wufoo.com](http://www.wufoo.com))
    * there are tons of free survey apps but this one is really pretty
* Campaign Monitor ([http://www.campaignmonitor.com](http://www.campaignmonitor.com))
    * easy to send one-off emails (without having to import and manage mailing lists)

### Ask Questions On a Website

* LaunchRock ([http://www.launchrock.com](http://www.launchrock.com))
    * make landing pages fast and encourage people to tweet about you
* Strikingly ([http://www.strikingly.com](http://www.strikingly.com))
    * make one-page mobile-friendly landing page fast
* Qualaroo ([http://www.qualaroo.com](http://www.qualaroo.com))
    * survey: stick it on your existing website to ask targeted questions on specific URLs
* Olark ([http://www.olark.com](http://www.olark.com))
    * live chat: stick it on your existing website and ask questions of people who show up

### TALK TO PEOPLE

* ScheduleOnce ([http://www.scheduleonce.com](http://www.scheduleonce.com))
    * give people a URL and let them schedule their own calls with you
* Calendly

### Take Notes

* Google Forms
    * I take notes directly into these forms and then Google submits them into an xls.  The xls is hard to read, which is a pain, but it does force you to keep all your notes in one place.  Great if you have multiple people conducting interviews.
* Dropvox ([https://itunes.apple.com/us/app/dropvox-record-voice-memos/id416288287?mt=8](https://itunes.apple.com/us/app/dropvox-record-voice-memos/id416288287?mt=8))
* QuaSkype Call Recorder (http://www.ecamm.com/mac/callrecorder/)

### Observe

* Usaura ([http://www.usuara.com](http://www.usuara.com))
    * quick ‘click testing’ type user testing
* Skype ([http://www.skype.com](http://www.skype.com))
    * if you need to screenshare, this seems like the most attainable for low-tech-savvy folks

### Measure

* Crazy Egg ([http://www.crazyegg.com](http://www.crazyegg.com))
    * stick it on your site to see where people click
* Optimizely ([http://www.optimizely.com](http://www.optimizely.com))
    * easy A/B testing

FYI things I’m not crazy about:

* UserVoice - too passive and biased towards the nutcases
* Ask Your Target Market - bait and switch, they say finding people is cheap but once you add criteria it becomes pricey
* Qualtrics - complicated, expensive surveys




Links:
* https://www.helpscout.net/blog/customer-survey/
* https://www.helpscout.net/blog/customer-feedback/
* [The Lean Toolbox](https://docs.google.com/document/d/1-MNatrbrJ9la-HHUzJW_i8x1GrNqe4DUMuuJ8UbvRE8/edit)
