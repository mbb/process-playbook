## Social Listening

Listening through social media can prove particularly useful for gathering candid feedback from customers. I refer to this method as "social listening" because direct comments or mentions on social networks aren't the only way for your business to get responses.  Engagement on social media can be overhyped, but in this instance social platforms can be utilized successfully and get customers involved.
