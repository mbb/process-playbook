## Exploratory Interviews

Can direct outreach really be beneficial in getting feedback from customers? Absolutely. Understanding your customers is often as easy as talking to them directly. 

* [When to Use](#when-to-use)
* [Write a Hypothesis](#write-a-hypothesis)
* [Writing non-biased questions](#writing-non-biased-questions)
   * [Effective questions](#effective-questions)
   * [Ask about the past not the future (the future is aspirational)](#ask-about-the-past-not-the-future-the-future-is-aspirational)
   * [Eliminate leading questions](#eliminate-leading-questions)
   * [Step outside your own perspective](#step-outside-your-own-perspective)
   * [Solicit 'impossible' problems](#solicit-impossible-problems)
* [How to take good notes during an interview](#how-to-take-good-notes-during-an-interview)
* [Setting the right interview tone](#setting-the-right-interview-tone)
* [Finishing up](#finishing-up)
   * [Debriefs](#debriefs)
* [How to distill to a sharable document](#how-to-distill-to-a-sharable-document)
* [Engaging with skeptical coworkers](#engaging-with-skeptical-coworkers)
* [Reaching Out Through Email](#reaching-out-through-email)
   * [A. The importance of a 'speed assurance'](#a-the-importance-of-a-speed-assurance)
   * [B. Keeping email feedback organized](#b-keeping-email-feedback-organized)
   * [C. The value of a personal email](#c-the-value-of-a-personal-email)

## When to use

When do they work best? What can you find out through interviews?

* Current behaviors
* Frustrations
* Constraints (what's limiting behavior)
* Decision-making factors

There are a couple important things to note when conducting these sorts of interviews, and the [following tips from the Nielsen group][18] can help you get started: 

* **Focus on user attitudes.** Explore how users think about a problem. Asking them what a button color should be will get you nowhere, but understanding their impressions ("This feature is too complicated") will allow you to alter features to address the problem.
* **Use the critical incident method.** Ask users to recall specific instances in which they faced a particularly difficult usage case or when something worked particularly well.
* **Inquire about habits.** Asking users how they normally do a task can reveal problems they didn't even know were there. If a user is jumping through four menus to do something that they could do with a shortcut, then you now have something to fix.

## Write a Hypothesis

It must be testable to be valid. Must include *people* who have a *problem* that, when solved, provides *value* to them. Remember that these interviews focus on user perspective so it must start from there benefits, not yours.

```
I believe _____ (this kind of person)
Needs to solve ____ (problem)
Which happens when ____ (situation)
And prevents ____ (value or capabilities)
```

## Writing non-biased questions

General practices:

* "Tell me how you do ______ today..."
* "Last time you did _________, what did you do to prepare? Afterwords to wrap up?"
* "If you had a new [coworker/friend] who was learning to do ______, what advice would you give them?"
* "If you could wave a magic wand and change anything - doesn't have to be possible - what would it do?"

Look for where people show emotions! That's everything. Once you've found it, ask:

* In the past month, how often has [problem] happened?
* What other ways have you tried to solve this [problem]? [1]
* If [problem] was magically solved, what would you be able to do differently? [2]
* How high of a priority is it to fix [problem]?
* Who else is affected when [problem] happens?

[1] If people don't try to solve this (workarounds, failed attempts), then they're unlikely to pay for a solution.
[2] Great for new feature requests. "That's a great idea. Just to make sure I understand: if you had this feature, what would you be able to do differently?"

### Effective questions

Avoid language that leads to ineffective questions. Examples:

```
Ineffective:
"Which project management tools do you use?"

Effective:
"How do you keep track of what you need to get done at work?"

I: "How long does your commute take?"  
E: "Tell me about how you get to work..."
```

### Ask about the past not the future (the future is aspirational)

```
I: How often do you go to the gym?
E: In the last month, how often did you go to the gym?

I: Is it difficult to find childcare when your kid is sick?
E: How much of a problem is it when your kid is home sick?
```

### Eliminate leading questions

```
Biased: Wouldn't it be good if your banking app was more secure?
Neutral: Tell me what you know about your bank's current security level?
```

### Step outside your own perspective

```
I: What do you find challenging about completing Task X? (Implies someone is not good at their job.)
E: What advice would you give a new coworker on tackling Task X? (They're the authority)
```

### Solicit 'impossible' problems

```
I: How would you make Product X better?
E: If you could wave a magic wand and change ONE thing - doesn't have to be possible - about doing Task X, what would it be?
```

You might find, as an expert, that you **can** do this.

## How to take good notes during an interview

* Recording makes it awkward from the start, plus you have to listen to it (average of 20 per project)
* Don't recommend laptop as an interviewer
* Invite a note taker (friend) and give them visual queues for:
  * Invalidates
  * Validates
  * Emotion
  * Surprise

## Setting the right interview tone

* Avoid boilerplate introductions that create a sterile environment
* Practice a strong, confident and reassuring starter conversation
* Reassure they're being very helpful (tap into that positive energy)

Points to hit:
* Thank you!
* Is now still a good time?
* I'm really trying to learn about this area and I'd like to understand your personal experiences in this day-to-day
* Nothing you say will be boring to me
* Do you have any questions for me?


## Finishing up

### Debriefs

* How did the interview start off?
* What were your leading questions? How can you reword those?
* Which questions need adjusting or rewording?
* What else should I do differently next time?

## How to distill to a sharable document

Has to be short, memorable and repeatable.

```
We went into this interview believing: (repeat hypothesis)
Here's what we heard that invalidates that:  _______
Here's what we heard that validates that:    _______
Here's where customers were really emphatic: _______
Here's what we're going to ask/do next: _______
```

## Engaging with skeptical coworkers

Don't disagree right away, work with their skepticism. Reach out to skeptics to invite them into the process.

* "Are you sure you talked to the right customers?"
* I'm not sure. We spoke with customers that exhibit this, this and this.
  * Does that align with your understanding of the customer?
  * Could you provide suggestions for better criteria?
* "If you're skeptical of the process, you can use this template and take notes. It's a small time commitment and will let you think whether you find this feedback as valid."


## Reaching Out Through Email

There is no question that email is one of the most valuable ways to gather candid customer feedback. 

However, there are a few ways you can improve the way customers reach out to you via email to maximize this channel's effectiveness, and all of these changes will create a better experience for customers, too. 

The three main elements you should focus on for soliciting feedback via email are:

* Assuring customers of a speedy response.
* Creating an organized customer feedback system.
* Sending candid follow-up emails.

### A. The importance of a 'speed assurance'

Recent [research][4] published by a United Kingdom-based customer experience group reveals two very startling statistics about customer feedback. 

1. Forty-three percent of those surveyed stated that they don't complain/leave feedback because they don't think the business cares. Is it any wonder that most companies [don't hear from unhappy customers][5]?
2. Of those same customers, 81 percent said they would be willing to leave feedback if they knew they would get a fast response. 

As shown by these statistics, if you want to ensure that you're hearing candid feedback from customers, the simple addition in your email of "We'll get back to you ASAP" will go a long way. 

### B. Keeping email feedback organized

In an earlier post covering our [workflow for managing feedback][6], I discussed how to use tools like [Trello][7] to create "boards" that your whole team can access and contribute to, ensuring that no good feedback slips through the cracks. 

That post covers our method in detail, but the takeaways are: 

* Create boards within Trello titled "Product Ideas" (feature requests), "Up Next" (what's being worked on) and "Roadmap" (what you plan to work on).
* Create individual cards within each board to categorize requests. For our Product Ideas board, we use sections like "Inbox" (new ideas), "Rejected" (discarded ideas), "Someday/Maybe" (good ideas, but not urgent), and "Apps" (integration requests).
* Add email addresses within cards for the people who requested the idea. For instance, anyone who asked us for [Reports][8] upgrades will be added to a list within a card so that they can be notified when the upgrade is complete. Here's an example card (with emails blocked out for privacy):

![Trello Screenshot][9]

This system lets you keep tabs on what's being requested and by whom, as well as tracking ideas you've already passed on. This also gives employees [a clear roadmap][10] for future customer interactions. 

### C. The value of a personal email

Sometimes the best way to get a candid response from a customer is to simply ask for one. 

When customers sign up via email to access information on the site, you have the opportunity to send out an auto-responder email that asks a single question. You can inquire about what customers are struggling with, what feature they'd like to see the most, or simply ask why they signed up. 

At the end of the email, you should ask them to reply to you; many will, and their responses will be candid. Since this channel is not public (like social media) and because the method is personal (unlike a survey), it can allow you to start some pretty interesting conversations with customers.

Just make sure you actually reply to these emails, or you'll be letting people down and they won't want to email you again. 


[1]: https://www.helpscout.net/blog/customer-satisfaction/
[2]: https://www.helpscout.net/embed-tools/
[3]: http://www.howto.gov/customer-experience/collecting-feedback/specify-goals
[4]: http://rantandrave.com/about/press-releases/rapide-research-are-brits-getting-their-just-desserts/
[5]: https://www.helpscout.net/blog/5-warning-signs-that-your-customer-service-sucks/
[6]: https://www.helpscout.net/blog/customer-feedback-systems/
[7]: https://trello.com/
[8]: https://www.helpscout.net/reports/
[9]: https://www.helpscout.net/images/blog/2013/feb/trello3.jpg
[10]: https://www.helpscout.net/blog/empowering-employess-without-losing-your-shirt/
[11]: https://www.helpscout.net/images/blog/2016/jun/5-16-survey.png
[12]: https://qualaroo.com/
[13]: http://www.surveymonkey.com/
[14]: https://www.helpscout.net/blog/customer-survey/
[15]: http://www.amazon.com/Rocket-Surgery-Made-Easy-Do-It-Yourself/dp/0321657292
[16]: http://www.usertesting.com/
[17]: http://blog.kissmetrics.com/best-ways-to-get-feedback/
[18]: http://www.nngroup.com/articles/interviewing-users/
[19]: https://join.me/
[20]: https://www.helpscout.net/images/blog/2016/jun/5-16-poll.png
[21]: https://www.helpscout.net/blog/quality-customer-service/
[22]: https://www.helpscout.net/images/blog/2016/jun/5-16-data.png
[23]: http://blog.kissmetrics.com/secret-to-great-feedback/
[24]: https://www.helpscout.net/images/blog/2016/jun/5-16-feedback.png
[25]: https://www.kissmetrics.com
