We have 5 minutes and a lot of details. To simplify, the presentation will have 3 parts: 
1. The setup
  * Story
  * Assertion
2. The visual
  * Graphic 
  * Demo
3. The call to action
  * Announcement 
  * Preview 
  * HugOps

