## Status - WIP 

You’re given a glass, some large rocks and a bag of peebles. The goal is to fit everything into the glass.

:: Spoiler alert ::
It’s an old trick. If you fill the glass with smaller rocks first, the big ones will not fit. If you place the large rocks in first, the smaller ones surround them, fitting everything nicely.

It’s an apt analogy to managing time throughout the day as long as you follow it through. We manage time to manage priority, not size, of a project. The large rocks are not your largest tasks, they’re your most important priorities.

Anchor the day in the most important times and the rest will fill in.

——



Rules I’ve been following:

1. put a number that comes to mind intuitively
2. Determine what assumptions where incorporated in that number
3. Did you miss any core assumptions? Write them down now
4. Now update your estimations
5. Did you do something fundamentally wrong? Estimate hours per day instead of hours per week? Start over.
6. Determine what is absolutely essential. Mark them as so in column B.


Other helpful rules (from http://students.mimuw.edu.pl/~zbyszek/posi/ibm/RUP_Eval/process/modguide/md_wdebnddlp.htm )

Keep the following estimation guidelines in mind:
* Assume one person (you) will perform all tasks.
* Assume all tasks will be performed sequentially; don't worry about sequencing and predecessor tasks at this time.
* Assume that you can devote uninterrupted effort to each task (this may seem absurdly optimistic, but it simplifies the estimation process).
* In units of calendar time, list any known waiting times you expect to encounter between tasks. This will help you translate effort estimates into schedule estimates later on.


Do your essentials add up to more than a week worth of hours? If so, you have two options: reduce duration by scheduling your work.

You can ignore inflation of duration if you’re willing to aggressively timebox the event
> scheduling on your calendar
> setting alarms to begin and end the task
> willing to stop the task in the middle of something <— that’s the biggest indicator of whether you can negate duration



Still over saturated for your week of 168 hours? If so, break tasks down to more specific sub-tasks.



This process has room for the inevitable interruption that is life.


What I love about wideband delphi:
* Removing an hour of work does not give you an hour back. You pay a tax on your time due to the multipliers. That’s how life works — pivots are always possible and always come at a cost. If it’s in software, we call that technical debt. If I were a writer, I’d call it preempting writer’s block. In the day-to-day trenches of life, we call that procrastination.

Other rules I’m testing out:
* No time block can be within 15 minutes of another time block
* Keep honest with your time blocks - don’t pad your time too much before or after. Part of this process discovers pockets of time you didn’t realize you had. Don’t hide them again.

Essentials
4 hour slots


Week 1 — Essentials w/Cleaning + Paperwork
Week 2 — Essentials w/Blogging
Week 3 — Essentials w/Getaway time with Adele



Blogging no longer becomes an accidental task
Neither does cleaning
Neither do get aways

“Blogging for me” is now a once a 3 week certainty. Variability in between is fine. If I get a breathe of inspiration like this vacation I’m on, then I’ll balance them out over some time.

If I want to blog more frequently, I’ll have to build it into the black-box that is my workday.

If I can’t, I can’t.

I know my priorities.


---
http://www.stellman-greene.com/aspm/images/ch03.pdf
Wideband Delphi Process[edit]
Barry Boehm and John A. Farquhar originated the Wideband variant of the Delphi method in the 1970s. They called it "wideband" because, compared to the existing delphi method, the new method involved greater interaction and more communication between those participating. The method was popularized by Boehm's book Software Engineering Economics (1981). Boehm's original steps from this book were:

1. Coordinator presents each expert with a specification and an estimation form.
2. Coordinator calls a group meeting in which the experts discuss estimation issues with the coordinator and each other.
3. Experts fill out forms anonymously.
4. Coordinator prepares and distributes a summary of the estimates
5. Coordinator calls a group meeting, specifically focusing on having the experts discuss points where their estimates vary widely
6. Experts fill out forms, again anonymously, and steps 4 to 6 are iterated for as many rounds as appropriate.
A variant of Wideband Delphi was developed by Neil Potter and Mary Sakry of The Process Group. In this process, a project manager selects a moderator and an estimation team with three to seven members. The Delphi process consists of two meetings run by the moderator. The first meeting is the kickoff meeting, during which the estimation team creates a work breakdown structure (WBS) and discusses assumptions. After the meeting, each team member creates an effort estimate for each task. The second meeting is the estimation session, in which the team revises the estimates as a group and achieves consensus. After the estimation session, the project manager summarizes the results and reviews them with the team, at which point they are ready to be used as the basis for planning the project.

* Choose the team. The project manager selects the estimation team and a moderator. The team should consist of 3 to 7 project team members. The team should include representatives from every engineering group that will be involved in the development of the work product being estimated.
* Kickoff meeting. The moderator prepares the team and leads a discussion to brainstorm assumptions, generate a WBS and decide on the units of estimation.
* Individual preparation. After the kickoff meeting, each team member individually generates the initial estimates for each task in the WBS, documenting any changes to the WBS and missing assumptions.
* Estimation session. The moderator leads the team through a series of iterative steps to gain consensus on the estimates. At the start of the iteration, the moderator charts the estimates on the whiteboard so the estimators can see the range of estimates. The team resolves issues and revises estimates without revealing specific numbers. The cycle repeats until either no estimator wants to change his or her estimate or the estimators agree that the range is acceptable.
* Assemble tasks. The project manager works with the team to collect the estimates from the team members at the end of the meeting and compiles the final task list, estimates and assumptions.
* Review results. The project manager reviews the final task list with the estimation team.
