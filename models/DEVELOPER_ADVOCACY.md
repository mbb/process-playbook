## Status - WIP

# Defining Developer Advocacy

## Overview

Developer Advocacy is a team that spans projects and teams to smooth the edges on the user experience.

* Advocacy as Sales Enablement
* Advocacy as Partner Enablement
* Advocacy as Product Engineering
* Advocacy as Technical Marketing
* Advocacy as Support Engineering


We provide a few core services to our teammates:

######

* Help produce technical content for users and developers of our projects
* Help prepare Calls for Proposal (CFPs)
* Improve user experience of our public projects

## What is Developer Advocacy?
Developer Advocacy is a practice that spans projects, and teams to smooth the edges on the user experience. It is a cross-disciplinary skill worker that can flow between:

* Tech Marketer
* Tech Writer
* Product Owner
* Community Manager
* (traditional) Marketer

![](../images/define-da.jpeg)

They help everyone be more successful by being someone who can:

* LEAD as an example of how to publicly contribute
* SPEAK as a user and developer of Intel software
* EMPOWER others to meaningfully contribute
* IMPROVE communication throughout the system

### Why DA?
Developer Advocates are key to accelerate project adoption through targeted contribution & internal coordination. They're accountable to the big picture view of SDI with a razor focus on results for the team.

## Advocacy In Practice

### Phases of Advocacy

Advocacy takes multiple forms at different times in a project. The **3 major phases** are:

1. Support Engineering - Determine what skill gaps exist on the project. DAs fill this need, help communicate and teaching others how to support it going forward. (Ex. Documentation)
1. Expand Audience - You can give a voice to this project whether you submit CFPs, write blog posts, share on Twitter or share demos on GitHub. Do everything you can. (Ex. Slack channel + coaching)
1. Adjust to Business Need - None of this matters without alignment to a business object. Form partnerships across your organization. Align, then document progress. (Ex. customer stories)

### Work in Progress (WIP) Management
Developer Advocates must balance the types of work they take. I do so using a [Kanban](/processes/KANBAN.md) in order to:
* Limiting Work in Progress
* Limit similar types of work done simultaneously (because it's about output, not throughput)
* Recognizing that a deep backlog of one type of work is a sign that a team needs to hire for a specific skill (e.g. Tech Writer, Product Owner, Community Manager)

### Supporting Conference Proposals (CFPs)
The Developer Advocacy team is here to support the team's goal of effectively presenting our story. Submissions to Call For Papers (CFP) give us an opportunity to do so.

| What we do                                       | What we don't do                                                            |
|:-------------------------------------------------|:----------------------------------------------------------------------------|
| Notify everyone of upcoming CFP due dates        | Provide permission to speak or attend conferences (speak with your manager) |
| Collect submitted CFPs you can use for reference | Decide what events we sponsor vs don't sponsor (that's <this> team)         |
| Provide feedback on your proposal                |                                                                             |
| Help you practice your presentation              |                                                                             |
| Help you make your slides pretty                 |                                                                             |

If you would like help working on slides, tag a team member in the #speaker-prep channel on Slack.
