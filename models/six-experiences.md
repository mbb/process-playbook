By “experience” I mean the full cross section. What IBM calls the “six experiences”. These include:
* Discover, Try and Buy -How do people find, understand and acquire the product? That may mean buy from a shop, it may mean sign up online.
* Getting Started - What is the first use experience for the product? That includes first set it up, or learning how to use it.
* Productive Use - The day to day use of the product from day 2 to day 100. Including both occasional use and power users.
* Manage and Upgrade - What do users need to do to keep the product or service going smoothly, and make changes to their usage — add team members, change their subscription and so forth.
* Leverage and Extend - How do users take the product as is, and combine it with other products or services or make customization to it? That might mean using APIs for example to develop extensions.
* Get Support - If users run into problems either of their own making or due to product deficiencies, how do they get help?


Reference:
https://medium.com/startup-study-group/how-to-combine-design-thinking-and-agile-in-practice-36c9fc75c6e6
