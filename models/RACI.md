## Using RACI
I like to use RACI, modified to RASCIO, to visualize who's taking lead on key launch choices.

### When to (not) use it
This model is designed to provide clarity across teams. My soft rules on RACI is anything that takes (1) less than a week or (2) has less than 3 teams involved does not need this heavy process. If you have both a longer-term project and many teams involved, you can save yourself from hours, and your company from hundreds of hours, of meetings with this relatively simple process.

### How it works
Begin by listing out all crucial events throughout the launch process and the teams that will be touched by this decision (both involved and have repercussions from it). I've populated a list of events I use as a baseline to any of these projects.

After the events and teams are listed, add one of the following values to each column:

* **Responsible** - The person who does the work to achieve the task. They have responsibility for getting the work done or decision made. As a rule this is one person.
* **Accountable** - The person who is accountable for the correct and thorough completion of the task. This must be one person and is often the project executive or project sponsor. This is the role that responsible is accountable to and approves their work.
* **Supporting** - The people who provide additional cycles for the project to complete tasks.
* **Consulted** - The people who provide information for the project and with whom there is two-way communication.
* **Informed** - The people who are kept informed about progress and with whom there is one-way communication. These are people that are affected by the outcome of the tasks so need to be kept up-to-date.
* **Omitted** - Those omitted from the process (not always necessary, but nice to be clear about expectations)
