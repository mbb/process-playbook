# Kanban Practices

Basic rules:
* Visualize work
* Limit work in progress (WIP)

Handy strategies:
* Ownership (One per task)
* Pairing (if teamwork could reduce time to completion)
* Swarm (sent the whole team to a  war room on a critical task)

Goal:
* Reach predictable throughput (flow) defined by time to completion (often an SLA)

## Types of Work

Work is defined by the expected results and time to delivery.

* Standard - All tasks, be default, must have a result that aligns with our Principles unless specifically noted. Timeline of all work should take no more than 4 work weeks to complete. Weekly checkins along with strict WIP limits will assure they progress.
* Spike - Tasks that begin with the term "Spike:" do not require results as an outcome. These are considered strategic investments of time and energy. Spikes can take up to 2 weeks and then will be closed as completed.

## Guidelines

### Basic Flow

1. Have a WIP limited kanban board: <link>
1. When a ____ is ready to take a task:
1. Look at "Ready for Review" item can be helped to complete
1. Next, review items that are "In Progress" to see:
    1. If you can help another ____ that has more than one item of work assigned to them
1. Choose a task (assigned or unassigned) from the "Ready to Pull" column
1. Assign the task to yourself and move to "In Progress" (and through the rest of the workflow accordingly)
1. If there are no tasks that are "Ready to Pull", notify the team in the #slack-channel Slack channel
1. If there is no immediate response, schedule a Grooming Session

**Note:** If a task requires action from an external team, i.e MKT, ENG, BIZ, etc team member, move the task to the "The Pen" column and assign the issue to the appropriate team member for the issue. If the team member is unknown, assign it to the team lead for the respective project (see Wiki space for the particular project if the team lead is unknown). Add a comment in issue with some more details.

### Urgency

By definition, urgent requests must be prioritized over other work. Due to this point, we cannot accept more than 1 urgent request per person in order to properly address the requirement. All other work is moved to "The Pen".

### Planning

Every Monday morning the team will hold a planning/grooming session.
Most importantly:
* Review work in progress
    * How long is it taken from Ready to Done?
    * What's stopping it from being Done?

Secondarily, if the Ready column is less than 3 items:
* Invite issue creators, Developer Advocates and related stakeholders to a grooming session
* Discuss prioritizing of request
* Move work from backlog into Ready
* Pull new work into In Progress within WIP limits

### Goal: Get to Predictable Throughput

Goal of providing quality output in a predictable timeline consistently
S
M
L
4 days
1 month
3 months
