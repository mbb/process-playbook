**Note:** You want this process if you:

* Want to share code publicly through GitHub
* Want it to have a mascot (or other imagery)
* Want to design for successful public use

# Process to Open Source
You have a project. Now what?

![](../images/open-source-stages.png)

Expectations for how we do what we do changes throughout the process. Here's my best effort to document those changes and the lessons learned from Open Source efforts, with some bear traps to keep in mind no matter what phase you're in.

## Early Development (from git init to 3 months from public)

There are practices to consider throughout the development cycle starting from day one.

### Talking about your project

* Do not share context of the project outside the company without ____'s approval
* Leave other company logos outside of your presentation
* Be sensitive to the story - always answer the "why should we care about it?" question
<< Rules based on teams sensitivity >>

### Branding your project

Everything you do, from naming conventions of methods to your project name itself, are NOT FINAL. Remember that upfront.

* **Naming** - Write your code in a way where naming can change without ruining its theme. For instance, changing to Snap, from Pulse, was a search and replace. It took some time, but it could have been worse. Don't plan to keep your naming convention from day zero through to Open Source
* **Mascot** - You can use themed images, like the Snappy Turtle, early on, but don't plan for it to be the final image

### Code commits

Remember that every line of code and every commit message is easy to find. Be conscious of this point from day one. No one wants to rebase because someone's 2am commit message has an HR violation in it and was found by a customer (this is from actual experience from Matthew Broberg - feel free to ask :smile:).

### Documenting your project
At this stage in the game, you can be pretty minimal about your documentation. That said, any work you do to capture design decisions will save you a lot of headache down the road. The more state diagrams, key concepts and workflows you can outline, the less you have to do right before open sourcing.

* **Designing a /docs folder** - The convention we've used is to have a /docs folder on your GitHub repository. You then have a /docs/design folder with details of the project. [See Kubernetes](https://github.com/kubernetes/kubernetes/tree/master/docs) for examples.

## Preparation (from 3 months to 1 week before public)
You are months away from announcing this project. There are a ton of bear traps to keep in mind. Pay close attention to the softer parts of your project and recruit others to help you launch smoothly!

### Talking about your project

At this stage, we need to be more aware of how we effect the greater Intel ecosystem and the teams that expect us to run our ideas by them. In short:
* Have a "Golden Deck" which will be used to present the project
* Have it reviewed by Marketing and Legal
* DA team will help manage this relationship

### Documenting your project

It's game time. You gotta do this now. You MUST have:
* README.md with an explanation of what the project does, why someone may use it, how to use it and how to give back
* LICENSE file
* Consistent labels in your GitHub repo
* A GitHub team of maintainers that will continue to maintain the project
* Correct status on your Issues for each issue

You SHOULD have:
* `docs/` folder with all user documentation your users may want (from API guides to configuration recommendations)
* A compelling mascot
* A github.io landing page with some further content
* Packaging strategy
* Announcement blog post prepared to publish to Medium
* < more here

If you haven't already, start to modify your practices at this stage to mirror what you want your external contributors to do. If you write uninformative commit messages in huge blocks of code merged in a PR, your public contributors will follow. If you need help framing this out, take the existing CONTRIBUTING.md file from Snap as a template and adjust it as necessary for your own use.

## Open Source (1 week before public to flipping the switch)
The moment is nearly here. 

### WARNING: Disclosure Time
Most executive team requires a disclosure before public announcement to warn partners and other execs about potential conflicts with other tools, potential users and potential partners. THIS IS THE MOST IMPORTANT STEP BEFORE PUBLIC. Make sure your management and DA team is ready to share one.

### Public branding and adoption
If you did the last step correctly, you should have a cool mascot, great icon and healthy slide deck to offer up. We can and should share it internally to marketing so they can use their channels to help amplify our public message.  

### Final touches on GitHub

In lockstep with the disclosure timeline, we will prepare for the repositories to be switched from private to public. To do so, all repos need everything mentioned above as well as:

* turned off Wiki on the repository settings
* review the correct people has write access to the project
* < more >
