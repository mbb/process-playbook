
1. Know what your project does 
2. Know who your project is for 
3. Mindmap  
4. Vet your top 5 
    1. Depending on your company, start by verifying there isn't a major project/initiative inside by the same name 
    2. Search online. I focus on GitHub. If you come across the same name, consider it a conflict if at least 2 out of 3 are true: 
        1. It has over 1000 stars 
        2. It's still maintained (within the last 2 years)
        3. It is targeted toward an overlapping audience to your own
5. Map out any possible dangerous to the name - translations, slang, etc 
6. Map out all the advantages to branding - mascot ideas, stickers, t-shirt designs
7. Share the top 5 with the team to weigh in, but not to make the final decision 
