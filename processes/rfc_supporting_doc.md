
## What is an RFC?
We use a document called a Request For Comments (RFC) as a way to share information across teams and gain insight from all those involved. RFCs are used to discuss proposed architectural and organizational improvements to our projects. We loosely base our RFCs on the Internet Engineering Task Force’s practice ([EITF practice](https://www.ietf.org/rfc.html)).

## Should I Open an RFC?
To know whether or not you need an RFC for your suggestion, ask yourself: Would a contributor care how this work gets done?

* If a user cares how it's done, follow this link to a blank copy of our template
* If they only care that it gets done, you can skip the RFC process

Don't need an RFC but still want feedback? You can always set up a grooming session!

## Effective RFCs
We use a template to ensure consistent and effective RFCs. This template gives space for the following key details:

* Length of time RFC will be open for comments
* Required participants in the discussion
* Your proposal of work to be done
* Work that is in/out of scope if additional detail is needed

< Define where to find the template >

## Life Cycle of an RFC
All of our RFCs will be open internally first in Google Docs, then moved to GitHub after a defined amount of time and input from internal stakeholders. The following image shows the timeline of an RFC:

![](../images/rfc-workflow.png)

## How Long Should My RFC Be Open?
RFCs need a clear and defined cut off dates for commenting. Our team standard is to have RFCs open in Google Docs for 2 weeks then move the conversation to Github where it should remain open for discussion for another 2 weeks. The amount of time an RFC is open varies on the project's complexity and urgency.

We recommend a minimum of 1 week and a maximum of 2 months based on the guidelines below:
* 2 days for urgent (requires approval)
* 2 months for low urgency or complex topics
* 2 weeks as default

## How to Track an RFC
All RFCs should have 2 JIRA issues associated with them:

* The first will be a story for creating and writing up your proposal, this may be as part of a SPIKE or as it's own issue.
* The second will be a task for monitoring your RFC. This task will span multiple sprints and will close once the RFC has closed.

### We are open to suggestions
The purpose of the template is to provide consistency to how we make Request For Comments in order to align internally so that we have a more productive conversation externally. We made this as simple as we could, but would love to improve it. Suggest your improvements <here> or message <x>.
