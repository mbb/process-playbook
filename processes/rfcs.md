# Designing an RFC Process

This is a template to provide consistency to how we make a Request For Comment in order to align internally so that we have a more productive conversation externally. We made this as simple as we could, but would love to improve it. Suggest your improvements __<link>__.

Below is the suggested timeline for an RFC. Before creating your RFC, it is crucial to understand why and how we use RFCs. For a refresher please checkout <link>.

## RFC Template: Fill in this section first
### Important Dates
Date Opened Internally:
Desired Date to Post Externally (“Date Open” on GitHub):
We recommend a minimum of 1 week and a maximum of 2 months

### Checklist
Update this checklist as the RFC progresses (mark with an ‘x’):  
- [ ] Defined Timeline
- [ ] Completed write up below (easiest if written in Markdown syntax)
- [ ] Notified team in #dev channel and tag required participants
- [ ] Added link to RFC in corresponding JIRA issue
- [ ] Consensus reached with required participants
- [ ] Asked for more feedback and hosted meetings to discuss as needed

After ‘Desired Date to Post Externally’:  
- [ ] Removed any private or confidential information before posting to GitHub
- [ ] Posted to GitHub using the same template
- [ ] Labeled GitHub issue with `type/rfc`

### Guidelines
For “Date to Close,” (on GitHub) use these sane defaults:
* 2 days for urgent (requires approval)
* 2 months for low urgency (or complex topics)
* 2 weeks as default

## Below this line is for GitHub
// to be put as a title in the GH issue
RFC Title:

**Date Open**: (‘Desired Date to Post Externally’ from above)
**Date To Close**:
**Required Participants**:

**Summary**: (2-3 sentences)
**Proposal**:

// If this needs clarification, include:
* **In Scope**:
* **Out of Scope**:
