- [x] Write up RFC
- [x] Open Source process
- [x] Launch prep guides
- [ ] Scoping Dev Advocacy docs
- [ ] Clean up Developer Advocacy in models
- [ ] Measure Community
- [ ] Social platforms
- [ ] Add more Quotables

# Process Playbook
I've found a repeatable set of processes that help me succeed at a company. Here is my plan to record these in version control and Markdown at the same time.

## Plan
Keep a portfolio of common processes here. The pieces fall into:

* models - great models to help think through work
* pitches - answering the question of how do you get adoption?
* processes - specific tactics for processes
* runbooks - specific tactics for particular types of work

## Quotable

Use this section to document how to make great arguments regarding processes.

### Manage work by throughput, not capacity

Like traffic, work doesn't fit. It flows. Capacity is a spatial relationship, throughput is a flow relationship. Throughput prioritization and completion bests rigorous upfront with planning.

### let people talk

I encourage you to take a long look at the questions you ask throughout the sales process and analyze whether you are giving your prospect a chance to talk enough.

* If not, consider asking open-ended questions which allow your prospect to share more detail. For example:
* “What inspired you to look for a new _____________ in the first place?”
* “Tell me about some of the challenges you have with your current ____________.”
* “Tell me how this ______________ compares to your current ____________.”
* “How do you see yourself enjoying your new ______________? ”

http://jeffshore.com/2015/03/ask-amazingly-powerful-sales-questions/

### robustness principle - "be conservative in what you do, be liberal in what you accept from others"

General design guideline for software:

Be conservative in what you do, be liberal in what you accept from others (often reworded as "Be conservative in what you send, be liberal in what you accept").
The principle is also known as Postel's law, after Internet pioneer Jon Postel, who wrote in an early specification of the Transmission Control Protocol that:

    TCP implementations should follow a general principle of robustness: be conservative in what you do, be liberal in what you accept from others.

https://en.wikipedia.org/wiki/Robustness_principle

### Theory of constraints  


### Blameless Retrospective

“Post Event Retrospective” over “Post Mortem”

Blameless Retrospectives:
     https://www.rallydev.com/blog/engineering/post-event-retrospective-part-i
